package ClientServer;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Client {

  private static SocketChannel client;
  private static ByteBuffer buffer;
  private static Client instance;


  public static String[] setMessage() {
    String[] message;
    Scanner sc = new Scanner(System.in);
    List<String> list = new ArrayList<>();
    String nextLine = sc.nextLine();
    while (!nextLine.equalsIgnoreCase("end")) {
      list.add(nextLine);
      nextLine = sc.nextLine();

    }
    message = new String[list.size()];
    message = list.toArray(message);
    return message;
  }

  public static Client start() {
    if (instance == null) {
      instance = new Client();
    }

    return instance;
  }

  public static void stop() throws IOException {
    client.close();
    buffer = null;
  }

  private Client() {
    try {
      client = SocketChannel.open(new InetSocketAddress("localhost", 5555));
      buffer = ByteBuffer.allocate(1024);
    } catch (IOException e) {
      System.out.println("Exception: " + e.getMessage());
    }
  }

  public String sendMessage(String msg) {

    buffer = ByteBuffer.wrap(msg.getBytes());
    String response = null;
    try {
      client.write(buffer);
      buffer.clear();
      client.read(buffer);
      response = new String(buffer.array()).trim();
      System.out.println("response = " + response);
      buffer.clear();
    } catch (IOException e) {
      System.out.println("Exception: " + e.getMessage());
    }
    return response;

  }


}
