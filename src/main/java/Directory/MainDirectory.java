package Directory;

import java.io.File;

public class MainDirectory {

  public static void main(String[] args) {
    File file = new File("src/main/resources/directory");
    DirectoryDemo.showDirectoryContent(file);
  }

}
