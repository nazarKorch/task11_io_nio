package Directory;

import java.io.File;
import java.nio.file.Path;

public class DirectoryDemo {

  static String space = "";

  public static void showDirectoryContent(File file) {

    if (file.isDirectory()) {
      System.out.println(space + file.getName() + "/");
      space = space + " ";
      File[] files = file.listFiles();
      for (File f : files) {
        showDirectoryContent(f);
      }

    } else if (file.exists()) {
      System.out.println(space + file.getName());

    }


  }

}
