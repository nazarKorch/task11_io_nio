package SerializeDemo;

import static SerializeDemo.ShipColors.*;

import java.io.Serializable;

public class Ship implements Serializable {

  private int numberOfPlaces = 350;
  private String name = "black pearl";
  private boolean isCargo = true;
  private transient ShipColors color = BLACK;

  public ShipColors getColor() {
    return color;
  }
  public String getName(){
    return name;
  }

}
