package SerializeDemo;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Main {

  public static void main(String[] args) {


    Ship shipOut = new Ship();
    Ship shipIn = new Ship();
    try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(
        new FileOutputStream("src/main/resources/serialize/ship.txt"))) {
      objectOutputStream.writeObject(shipOut);
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }

    try (ObjectInputStream objectInputStream = new ObjectInputStream(
        new FileInputStream("src/main/resources/serialize/ship.txt"))) {
      shipIn = (Ship) objectInputStream.readObject();
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }

    System.out.println("ship after serialization: name: " + shipIn.getName()
        + ", color: " + shipIn.getColor());
  }

}
