package FileDemo;

import java.io.File;

public class MainReader {

  public static void main(String[] args) {

    File file = new File("src/main/resources/reading/eng.pdf");
    ReaderDemo.readFromFile(file);
    BufferedReaderDemo.readFromFile(file);

  }

}
