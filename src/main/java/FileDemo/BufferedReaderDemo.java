package FileDemo;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.Reader;

public class BufferedReaderDemo {

  public static void readFromFile(File file) {
    if (file.exists()) {
      try (InputStream input = new BufferedInputStream(new FileInputStream(file))) {
        int count = 0;
        int data = input.read();
        while (data != -1) {
          data = input.read();
          count++;
          if (count == 1000000) {
            count = 0;
            System.out.println("buffered reading...");
          }
        }
      } catch (Exception e) {
        System.out.println(e.getMessage());
      }

    } else {
      System.out.println("file doesn't exist");
    }

    System.out.println("buffered reading ended");
  }
}


