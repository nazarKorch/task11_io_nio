package SourceCode;

import java.io.File;

public class MainCommentReader {

  public static void main(String[] args) {
    File file = new File("src/main/resources/code/source-code.txt");
    CommentReader.readCommentsFromFile(file);
  }

}
