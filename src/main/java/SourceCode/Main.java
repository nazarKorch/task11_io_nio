package SourceCode;

import ClientServer.Client;
import ClientServer.Server;

public class Main {

  public static void main(String[] args) {

    try {
      Process server = Server.start();
      Client client = Client.start();
      System.out.println("Enter your message(to exit enter END):");
      String[] messeges = Client.setMessage();
      for(String s: messeges){
        String str = client.sendMessage(s);
      }

      server.destroy();
      Client.stop();
    }catch (Exception e){
      e.printStackTrace();
    }
  }

}
