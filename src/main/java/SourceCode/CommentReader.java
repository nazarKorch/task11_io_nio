package SourceCode;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.Reader;


public class CommentReader {

  public static void readCommentsFromFile(File file) {

    if (file.exists()) {
      try (Reader reader = new BufferedReader(new FileReader(file))) {

        char[] data = new char[(int) file.length()];
        boolean notEndOfComment = true;
        StringBuilder com = new StringBuilder();
        String s = "";
        reader.read(data);

        for (int i = 3; i < data.length; i++) {

          if ((data[i - 1] == '*') && (data[i - 2] == '*') && (data[i - 3] == '/')) {

            while (notEndOfComment) {
              if (((data[i]) == '*') && ((data[i + 1]) == '/')) {
                notEndOfComment = false;
              }
              if (data[i] != '*') {
                com.append(data[i]);
                i++;
              } else {
                i++;
              }
            }
            notEndOfComment = true;
          } else if ((data[i - 2] == '/') && (data[i - 3] == '/')) {
            while(data[i-1] != '\n'){
              com.append(data[i-1]);
              i++;
            }
            com.append(data[i-1]);
          }

        }
        System.out.println(com);


      } catch (Exception e) {
        System.out.println(e.getMessage());
      }

    }


  }

}
